package com.luxoft.rcpapp.util;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StudentValidatorTest extends StudentValidator {

	private List<String> nameNotValidList;
	private String nameValid ;

	private List<String> addressNotValidList;
	private String addressValid ;

	private List<String> cityNotValidList;
	private String cityValid ;

	private List<String> resultNotValidList;
	private String resultValid ;

	
	private void setNames() {
		nameNotValidList = new ArrayList<>();
		
		nameNotValidList.add("");
		nameNotValidList.add(" ");
		nameNotValidList.add("Ivan ");
		nameNotValidList.add("IvanIvanov");
		
		nameValid = "Ivan Ivanov";
	}
	
	private void setAddresses() {
		addressNotValidList = new ArrayList<>();
		
		addressNotValidList.add("");
		addressNotValidList.add(" ");
		addressNotValidList.add("Strit ");
		addressNotValidList.add("Strit, ");
		
		addressValid = "Street, 5";
	}

	
	private void setCitys() {
		cityNotValidList = new ArrayList<>();
		
		cityNotValidList.add("");
		cityNotValidList.add(" ");
		cityNotValidList.add("Dnepr 5");
		cityNotValidList.add("Dnepr5");
		
		cityValid = "Dnipro";
	}


	private void setResults() {
		resultNotValidList = new ArrayList<>();
		
		resultNotValidList.add("");
		resultNotValidList.add(" ");
		resultNotValidList.add("5 5");
		resultNotValidList.add("55");
		resultNotValidList.add("9");
		resultNotValidList.add("0");
		resultNotValidList.add("l");
		resultNotValidList.add("f");
		
		resultValid = "5";
	}

	
	@Before
	public void setUp() {
		setNames();
		setAddresses();
		setCitys();
		setResults();
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testIsNameValid() {
		assertTrue(StudentValidator.isNameValid(nameValid));
	}
	
	@Test
	public void testIsNameNotValid() {
		boolean actual;
		
		for (String name : nameNotValidList) {
			actual = StudentValidator.isNameValid(name);
			assertTrue(!actual);
		}
	}

	@Test
	public void testIsAddressValid() {
		assertTrue(StudentValidator.isAddressValid(addressValid));
	}

	@Test
	public void testIsAddressNotValid() {
		boolean actual;
		
		for (String address : addressNotValidList) {
			actual = StudentValidator.isAddressValid(address);
			assertTrue(!actual);
		}
	}
	
	@Test
	public void testIsCityValid() {
		assertTrue(StudentValidator.isCityValid(cityValid));
	}
	
	
	@Test
	public void testIsCityNotValid() {
		boolean actual;
		
		for (String city : cityNotValidList) {
			actual = StudentValidator.isCityValid(city);
			assertTrue(!actual);
		}
	}

	@Test
	public void testIsResultValid() {
		assertTrue(StudentValidator.isResultValid(resultValid));
	}
	
	@Test
	public void testIsResultNotValid() {
		boolean actual;
		
		for (String result : resultNotValidList) {
			actual = StudentValidator.isResultValid(result);
			assertTrue(!actual);
		}
	}

}
