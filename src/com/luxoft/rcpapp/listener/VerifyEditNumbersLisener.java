package com.luxoft.rcpapp.listener;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

public class VerifyEditNumbersLisener implements Listener {
	
	@Override
	public void handleEvent(Event e) {
		Text source = (Text)e.widget;
		
		String oldString = source.getText();
		String newString = oldString.substring(0, e.start) + e.text + oldString.substring(e.end);
		
		if (!newString.isEmpty()) {
			if ((newString.length() > 1) || (!newString.matches("\\d"))) {
				e.doit = false;
			} else {
				Integer value = Integer.valueOf(newString);
				if ((value == 0) || (value > 5)) {
					e.doit = false;
				}
			}
		}
	}

}
