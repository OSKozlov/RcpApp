package com.luxoft.rcpapp.listener;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

public class DisableInputTextListener implements Listener {

	@Override
	public void handleEvent(Event event) {
		event.doit = false;
		
	}

}
