package com.luxoft.rcpapp.listener;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchWindow;

import com.luxoft.rcpapp.dao.GroupDao;
import com.luxoft.rcpapp.dao.StudentDao;
import com.luxoft.rcpapp.dto.TreeObject;
import com.luxoft.rcpapp.dto.TreeParent;
import com.luxoft.rcpapp.model.GroupOfStudents;
import com.luxoft.rcpapp.model.Student;
import com.luxoft.rcpapp.util.StudentValidator;
import com.luxoft.rcpapp.view.GroupViewPart;

public class GroupViewCellModifier implements ICellModifier {
	
	private static Logger logger = Logger.getLogger(GroupViewCellModifier.class.getName());
	
	private final IWorkbenchWindow window;
	private boolean isModifyTreeParent;
	
	@Inject 
	private GroupDao groupDao;
	
	@Inject
	private StudentDao studentDao;

	public GroupViewCellModifier(IWorkbenchWindow window) {
		super();
		this.window = window;
		isModifyTreeParent = false;
	}

	@Override
	public boolean canModify(Object element, String property) {
//		System.err.println("### canModify ");
		
		if ((element != null) && (element instanceof TreeObject)) {
			TreeObject treeObject = (TreeObject) element;
			if (!treeObject.getName().equals(GroupViewPart.ROOT_FOLDER_NAME)) {
				return true;
			}
		}
		return false; 
	}

	@Override
	public Object getValue(Object element, String property) {
//		System.err.println("### getValue ");
		if ((element != null) && (element instanceof TreeObject)) {
			if (element.getClass() == TreeParent.class) {
				isModifyTreeParent = true;
			} else {
				isModifyTreeParent = false;
			}
			
			return ((TreeObject) element).getName();
			
		}
		return "";
	}

	
	private boolean updateStudentName(Integer id, String name) {
		boolean isUpdated = false;
		
		if (StudentValidator.isNameValid(name.trim())) {
			Optional<Student> student = studentDao.getByID(id);
			if (student.isPresent()) {
				student.get().setName(name);
				isUpdated = true;
			}
		} else {
			String errMsg = StudentValidator.getValidationMessage();
			logger.log(Level.WARNING, "An error occurred during rename tree object name", errMsg);
			MessageDialog.openError(window.getShell(), "Error", errMsg);
		}

		return isUpdated;
	}
	
	private boolean upDateGroupName(Integer id, String name) {
		boolean isUpdated = false;
		Optional<GroupOfStudents> group = groupDao.getByID(id);
		if (group.isPresent()) {
			group.get().setName(name);
			isUpdated = true;
		}
		return isUpdated;
	}
	
	
	@Override
	public void modify(Object element, String property, Object value) {
		boolean isUpdated = false;
//		System.err.println("### modify :" + value);
		if ((value != null) && (value instanceof String)) {
			String name = ((String) value).trim();
			if (!name.isEmpty()) {
				Object data = ((TreeItem) element).getData();
				if (data instanceof TreeObject) {
					TreeObject treeObject = (TreeObject) data; 
					if ((groupDao != null) && (studentDao != null)) {
						if (!isModifyTreeParent) {
							isUpdated = updateStudentName(treeObject.getId(), name);
							
						} else {
							isUpdated = upDateGroupName(treeObject.getId(), name);
						}
						if (isUpdated) {
							treeObject.setName(name);
						}
					}else {
		        		logger.log(Level.WARNING, "An error occurred to inject objects", "An error occurred to inject objects");
		    			
		    			MessageDialog.openError(window.getShell(), "Error", 
		    					"An error occurred to inject objects");
		        	} 
				}
				if (isUpdated) {
					((TreeItem) element).setText(name);
				}
			} else {
				logger.log(Level.WARNING, "An error occurred during rename tree object name", "Name is empty!");
				MessageDialog.openError(window.getShell(), "Error", "Name is empty!");

			}
		}
		
	}

}
