package com.luxoft.rcpapp.util;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.PlatformUI;

import com.luxoft.rcpapp.application.Application;

public class RcpAppFileLocator {
	private static Logger logger = Logger.getLogger(RcpAppFileLocator.class.getName());
	
	public static String getProjectWorkspace() {
		String installDir;
		URL url = null;
		
		try {
			url = FileLocator.resolve(Platform.getBundle(Application.PLUGIN_ID).getEntry("/resources"));
			
		} catch (IOException e) {
			
			logger.log(Level.WARNING, "Project directory", e);
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Project directory",
					"Could not get installation directory");
			return "";
		}

		installDir = url.getPath().trim();
		if (installDir.isEmpty()) {
			logger.log(Level.WARNING, "Project directory", "File path is empty");
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Project directory",
					"File path is empty");
		}
		
		if( Platform.getOS().compareTo(Platform.OS_WIN32) == 0 ) {
			installDir = installDir.substring(1);
		}
		
		return installDir;
	}

}
