package com.luxoft.rcpapp.util;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;

import com.luxoft.rcpapp.dto.TreeObject;
import com.luxoft.rcpapp.dto.TreeParent;
import com.luxoft.rcpapp.model.GroupOfStudents;
import com.luxoft.rcpapp.model.Student;
import com.luxoft.rcpapp.view.GroupViewPart;


@Creatable
@Singleton
public class TreeViewConverter {
	
	public TreeViewConverter() {
	}
	
	public TreeParent convertToTreeParent (GroupOfStudents groupOfStudents) {
		return new TreeParent(groupOfStudents.getId(), groupOfStudents.getName());
	}
	
	
	public TreeObject convertToTreeObject (Student student) {
		return new TreeObject(student.getId(), student.getName());
	}
	
	public TreeParent convertGroupsToDto(List<GroupOfStudents> groups) {
		TreeParent treeParentMain = new TreeParent(0, GroupViewPart.ROOT_FOLDER_NAME);
	
		List<TreeParent> treeParents = groups.stream()
				.map(this::convertToTreeParent)
				.collect(Collectors.toList());
				
		for (TreeParent treeParent : treeParents) {
			treeParentMain.addChild(treeParent);
		}
		
				
		TreeParent root = new TreeParent(0, "");
		root.addChild(treeParentMain);

		return root;
	}
	
	
	private List<TreeObject> getTreeObjectbyStudentsInGroup (Integer groupId, List<Student> students) {
		
		return students.stream()
				.filter(student -> student.getGroup().get().getId().equals(groupId))
				.map(student -> convertToTreeObject(student))
				.collect(Collectors.toList());
	}
	
	
	public TreeObject convertStudentsToDto(TreeParent parent, List<Student> students) {
		
		TreeObject [] root = parent.getChildren();
		
		for (TreeObject treeObject : root) {
			if (treeObject instanceof TreeParent) {
				TreeObject [] treeParentMain = ((TreeParent) treeObject).getChildren();
				for (TreeObject treeGroupObject : treeParentMain) {
					if (treeGroupObject instanceof TreeParent) {
						TreeParent parentGroup = (TreeParent) treeGroupObject;
						Integer groupId = parentGroup.getId();
						parentGroup.addChildren(getTreeObjectbyStudentsInGroup(groupId, students));
					}
				}
			}
			
		}
	
		return parent;
	}
}
