package com.luxoft.rcpapp.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudentValidator {
	private static final String FIELD_IS_NULL = "field is null";
	private static final String FIELD_IS_EMPTY = "field is empty";
	private static final String FIELD_NOT_CONTAIN_NUMBERS = "field must contain numbers";
	private static final String FIELD_CONTAIN_NUMBERS = "field contain numbers";
	
	private static final String NAME_FIELD_NOT_VALID = "Name field must contain first and last name";
	private static final String ADDRESS_FIELD_NOT_VALID = "Address field must contain the house number";
	private static final String RESULT_FIELD_NOT_CONTAIN_ONE_NUMBER = "Result field must contain only one number";
	private static final String RESULT_FIELD_NOT_IN_RANGE = "Result field should be in range from 1 to 5\\n";
	
	private static StringBuilder validationMessage = new StringBuilder();
	
	
	public static String getValidationMessage() {
		return validationMessage.toString();
	}
	
	public static boolean isNameValid (String name) {
		validationMessage.delete(0, validationMessage.length());
		
		if (name != null) {
			if ((!name.trim().contains(" ")) && (!name.matches("\\w"))) {
				validationMessage.append(NAME_FIELD_NOT_VALID).append("\n"); 
			}
		} else {
			validationMessage.append("Name ").append(FIELD_IS_NULL).append("\n"); 
		}
		
		return validationMessage.length() == 0;
	}
	
	public static boolean isAddressValid (String address) {
		Pattern pattern;
		Matcher matcher;
		validationMessage.delete(0, validationMessage.length());

		if (address == null) {
			validationMessage.append("Address ").append(FIELD_IS_NULL).append("\n");
		}
		pattern = Pattern.compile("\\d+");
	    matcher = pattern.matcher(address);
	    if(!matcher.find(0)) {
	    	validationMessage.append(ADDRESS_FIELD_NOT_VALID).append("\n");
//	    	return "Address field must contain the house number!\n";
	    }
		
	    return validationMessage.length() == 0;
	}
	
	public static boolean isCityValid (String city) {
		Pattern pattern;
		Matcher matcher;
		validationMessage.delete(0, validationMessage.length());

		if (city == null) {
			validationMessage.append("City ").append(FIELD_IS_NULL).append("\n");
		} else if (city.trim().isEmpty()) {
			validationMessage.append("City ").append(FIELD_IS_EMPTY).append("\n");
		}
		pattern = Pattern.compile("\\d+");
	    matcher = pattern.matcher(city);
	    if(matcher.find(0)) {
	    	validationMessage.append("City ").append(FIELD_CONTAIN_NUMBERS).append("\n");
//			return "City field contain numbers!\n";
		} 
		
	    return validationMessage.length() == 0;
	}
	
	public static boolean isResultValid (String result) {
		validationMessage.delete(0, validationMessage.length());

		if (result != null) {
			if (result.trim().isEmpty()) {
				validationMessage.append("Result ").append(FIELD_IS_EMPTY).append("\n");
			} else if (!result.matches("\\d")) {
				validationMessage.append("Result ").append(FIELD_NOT_CONTAIN_NUMBERS).append("\n");
			} else if (result.length() > 1) {
				validationMessage.append(RESULT_FIELD_NOT_CONTAIN_ONE_NUMBER).append("\n");
			} else {
				Integer value = Integer.valueOf(result);
				if ((value == 0) || (value > 5)) {
					validationMessage.append(RESULT_FIELD_NOT_IN_RANGE).append("\n");
				}
			}
		} else {
			validationMessage.append("Result ").append(FIELD_IS_NULL).append("\n"); 
		}
		
		return validationMessage.length() == 0;
	}
	
	
}
