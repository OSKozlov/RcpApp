package com.luxoft.rcpapp.model;

import java.io.Serializable;
import java.util.Optional;

public class Student implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3578703012747463223L;
	
	private Integer id;
	private String name;
	private String address;
	private String city;
	private Integer result;
	private GroupOfStudents group;
	private String imagePath;

	public Student() {
		id = 0;
		name = "";
		address = "";
		city = "";
		result = 0;
		group = null;
		imagePath = "";
	}

	public Student(Integer id, String name, String address, String city, Integer result, GroupOfStudents group) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.city = city;
		this.result = result;
		this.group = group;
		imagePath = "";
	}

	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public Optional<GroupOfStudents> getGroup() {
		return Optional.ofNullable(group);
	}

	public void setGroup(GroupOfStudents group) {
		this.group = group;
	}

	
	
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Student other = (Student) obj;
		if (address == null) {
			if (other.address != null) {
				return false;
			}
		} else if (!address.equals(other.address)) {
			return false;
		}
		if (city == null) {
			if (other.city != null) {
				return false;
			}
		} else if (!city.equals(other.city)) {
			return false;
		}
		if (group == null) {
			if (other.group != null) {
				return false;
			}
		} else if (!group.equals(other.group)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (result == null) {
			if (other.result != null) {
				return false;
			}
		} else if (!result.equals(other.result)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Student [id = " + id + ", name = " + name + ", address = " + address 
				+ ", city = " + city + ", result = " + result + ", group = "
				+ group + "]";
	}

	
	

}
