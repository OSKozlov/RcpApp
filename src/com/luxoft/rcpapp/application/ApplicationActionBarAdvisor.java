package com.luxoft.rcpapp.application;

import java.time.LocalDate;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.StatusLineContributionItem;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

import com.luxoft.rcpapp.action.AddNewStudentAction;
import com.luxoft.rcpapp.action.DeleteStudentAction;
import com.luxoft.rcpapp.action.LoadStudentsFromFileAction;
import com.luxoft.rcpapp.action.OpenStudentViewAction;
import com.luxoft.rcpapp.action.RenameTreeObjectNameAction;
import com.luxoft.rcpapp.action.SaveStudentsToFileAction;

public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	
	private IWorkbenchWindow window;
	
	private IWorkbenchAction exitAction;
	private IWorkbenchAction aboutAction;
	private AddNewStudentAction addNewStudentAction;
	
	private OpenStudentViewAction openStudentViewAction;
	private LoadStudentsFromFileAction loadStudentsFromFileAction;
	private SaveStudentsToFileAction saveStudentsToFileAction;
	private DeleteStudentAction deleteStudentAction;
	private RenameTreeObjectNameAction renameTreeObjectNameAction;
	
	private StatusLineContributionItem statusItem;

	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);
	}

	@Override
	protected void makeActions(IWorkbenchWindow window) {
		this.window = window;
		
		exitAction = ActionFactory.QUIT.create(window);
		register(exitAction);
		
		aboutAction = ActionFactory.ABOUT.create(window);
		aboutAction.setText("About Student info");
		register(aboutAction);
		
		addNewStudentAction = new AddNewStudentAction(window);
		ContextInjectionFactory.inject(addNewStudentAction, window.getService(IEclipseContext.class));
		register(addNewStudentAction);
		
		window.getService(IEclipseContext.class).set(AddNewStudentAction.class, addNewStudentAction);
		
		openStudentViewAction = new OpenStudentViewAction(window);
		ContextInjectionFactory.inject(openStudentViewAction, window.getService(IEclipseContext.class));
		register(openStudentViewAction);
		
		window.getService(IEclipseContext.class).set(OpenStudentViewAction.class, openStudentViewAction);
		
		loadStudentsFromFileAction = new LoadStudentsFromFileAction(window);
		ContextInjectionFactory.inject(loadStudentsFromFileAction, window.getService(IEclipseContext.class));
		register(loadStudentsFromFileAction);
		
		saveStudentsToFileAction = new SaveStudentsToFileAction(window);
		ContextInjectionFactory.inject(saveStudentsToFileAction, window.getService(IEclipseContext.class));
		register(saveStudentsToFileAction);

		deleteStudentAction = new DeleteStudentAction(window);
		ContextInjectionFactory.inject(deleteStudentAction, window.getService(IEclipseContext.class));
		register(deleteStudentAction);
		window.getService(IEclipseContext.class).set(DeleteStudentAction.class, deleteStudentAction);
		
		renameTreeObjectNameAction = new RenameTreeObjectNameAction(window);
		register(renameTreeObjectNameAction);
		
	}

	@Override
	protected void fillCoolBar(ICoolBarManager coolBar) {
		IToolBarManager toolBarManager = new ToolBarManager(SWT.FLAT | SWT.RIGHT);
		toolBarManager.add(loadStudentsFromFileAction);
		toolBarManager.add(saveStudentsToFileAction);
		toolBarManager.add(new Separator());
		toolBarManager.add(deleteStudentAction);
		toolBarManager.add(addNewStudentAction);
		toolBarManager.add(new Separator());
		toolBarManager.add(openStudentViewAction);
		coolBar.add(new ToolBarContributionItem(toolBarManager, "main"));
		
	}

	@Override
	protected void fillStatusLine(IStatusLineManager statusLine) {
		statusItem = new StatusLineContributionItem("CurrentDate");
		statusItem.setText("Date: " + LocalDate.now().toString());
		statusLine.add(statusItem);	
		
	}
	
	@Override
	public void dispose() {
		ContextInjectionFactory.uninject(addNewStudentAction, window.getService(IEclipseContext.class));
		ContextInjectionFactory.uninject(openStudentViewAction, window.getService(IEclipseContext.class));
		ContextInjectionFactory.uninject(loadStudentsFromFileAction, window.getService(IEclipseContext.class));
		ContextInjectionFactory.uninject(saveStudentsToFileAction, window.getService(IEclipseContext.class));
		ContextInjectionFactory.uninject(deleteStudentAction, window.getService(IEclipseContext.class));
		super.dispose();
	}

	
}
