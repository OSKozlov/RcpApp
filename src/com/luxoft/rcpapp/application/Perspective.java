package com.luxoft.rcpapp.application;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import com.luxoft.rcpapp.view.GroupViewPart;
import com.luxoft.rcpapp.view.StudentEditorPart;

public class Perspective implements IPerspectiveFactory {

	public static final String ID = "com.luxoft.rcpapp.perspective";

	@Override
	public void createInitialLayout(IPageLayout layout) {
		layout.setEditorAreaVisible(true);
		
		layout.addStandaloneView(GroupViewPart.ID, false, IPageLayout.LEFT, 0.25f, 
				layout.getEditorArea());
		
		IFolderLayout folderLayout = layout.createFolder("student info", IPageLayout.TOP, 0.75f,
				layout.getEditorArea());
		
		folderLayout.addPlaceholder(StudentEditorPart.ID + ":*");
		layout.getViewLayout(GroupViewPart.ID).setCloseable(false);
	}

}
