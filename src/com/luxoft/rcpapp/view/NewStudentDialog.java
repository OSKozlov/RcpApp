package com.luxoft.rcpapp.view;

import java.util.Optional;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.luxoft.rcpapp.model.Student;

public class NewStudentDialog extends Dialog{

	private NewStudentViewComposite newStudentViewComposite;
	private String groupName;
	private Student newStudent;
	
	public NewStudentDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Add new student");
		
	}

	
	@Override
	protected Control createDialogArea(Composite parent) {
		newStudentViewComposite = new NewStudentViewComposite(parent, SWT.NONE);
		newStudentViewComposite.init(Optional.ofNullable(groupName));
		
		return newStudentViewComposite;
	}
	
	public void setStudentGroup (String nameGroup) {
		this.groupName = nameGroup;
	}

	@Override
	protected void okPressed() {
		 
		 if (newStudentViewComposite.isDataValid()) {
			 newStudent = newStudentViewComposite.getStudentFromView();
			 super.okPressed();
		 } else {
			 MessageDialog.openError(getShell(), "Wrong data", 
					 newStudentViewComposite.getNotValidMessage());
		 }
	}

	public Student getNewStudent() {
		return newStudent;
	}

}
