package com.luxoft.rcpapp.view;

import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.luxoft.rcpapp.dto.StudentEditorInput;
import com.luxoft.rcpapp.model.Student;

public class StudentEditorPart extends EditorPart {
	
	public static String ID = "org.luxoft.rcpapp.editors.student";
	
	ExistStudentViewComposite existStudentViewComposite;
	Optional<Student> student;

	public StudentEditorPart() {
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
		if (getEditorInput() instanceof StudentEditorInput) {
			student = Optional.ofNullable(((StudentEditorInput) getEditorInput()).getStudent());
			if (student.isPresent()) {
				setPartName(student.get().getName());
			}
		}

	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		existStudentViewComposite = new ExistStudentViewComposite(parent, SWT.NONE);
		existStudentViewComposite.init(student);
	}

	@Override
	public void setFocus() {
		existStudentViewComposite.setFocusAtStartUp();

	}

}
