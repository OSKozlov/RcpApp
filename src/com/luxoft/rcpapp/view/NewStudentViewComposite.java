package com.luxoft.rcpapp.view;

import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;

import com.luxoft.rcpapp.util.RcpAppFileLocator;

public class NewStudentViewComposite extends AbstractStudentViewComposite {
	
	private static final String LOAD_PICTURE_BUTTON_NAME = "  Load photo  ";
	
	private Button loadPictureButton;


	public NewStudentViewComposite(Composite parent, int style) {
		super(parent, style);
	}
	
	
	public void init(Optional<String> groupName) {
		initStudentViewComposite();
		
		initButtons();
		setGroupNameForNewStudent(groupName);
    	String defaultImagePath = RcpAppFileLocator.getProjectWorkspace() + ImageKeys.EMPTY_PHOTO.getStringValue();
    	displayStudentPhoto(defaultImagePath);
    	initListeners();
	}
	
	private void initButtons() {
		GridData gridData;
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_END | GridData.VERTICAL_ALIGN_END);
        gridData.horizontalSpan = 3;
//        gridData.horizontalIndent = 0;
        gridData.verticalIndent = 10;
        gridData.grabExcessHorizontalSpace = true;
        loadPictureButton = new Button(this, SWT.NONE);
        loadPictureButton.setText(LOAD_PICTURE_BUTTON_NAME);
        loadPictureButton.setLayoutData(gridData);
        
        loadPictureButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
		        fd.setText("Load student photo...");
		        fd.setFilterPath(RcpAppFileLocator.getProjectWorkspace());
		        String[] filterExt = { "*.gif", "*.*" };
		        fd.setFilterExtensions(filterExt);
		        String imagePathFromDialog = fd.open();
		        displayStudentPhoto(imagePathFromDialog);
			}
        	
		});
	}

	private void initListeners () {
		initListenersForNewStudent();
	}
	
}
