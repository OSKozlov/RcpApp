package com.luxoft.rcpapp.view;

public enum ImageKeys {
	GROUP_OF_STUDENTS("icons/group.gif"),
	STUDENT("icons/student.gif"),
	STUDENT_32("icons/student32.gif"),
	ROOT_FOLDER("icons/root.gif"),
	
	LOAD_FROM_FILE("icons/load.gif"),
	SAVE_TO_FILE("icons/save.gif"),
	DELETE_STUDENT("icons/delete.gif"),
	ADD_STUDENT("icons/add.gif"),
	
	EMPTY_PHOTO("empty.gif");
	
	
	private String stringValue;
	
	private ImageKeys(String stringValue) {
		this.stringValue = stringValue;
	}

	public String getStringValue() {
		return stringValue;
	}
	
	
}
