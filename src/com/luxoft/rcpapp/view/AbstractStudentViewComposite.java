package com.luxoft.rcpapp.view;

import java.io.File;
import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.luxoft.rcpapp.listener.DisableInputTextListener;
import com.luxoft.rcpapp.listener.VerifyEditNumbersLisener;
import com.luxoft.rcpapp.model.Student;
import com.luxoft.rcpapp.util.RcpAppFileLocator;
import com.luxoft.rcpapp.util.StudentValidator;

public abstract class AbstractStudentViewComposite extends Composite {

	private static final String NAME_STUDENT_LABLE_NAME = "Name";
	private static final String GROUP_STUDENT_LABLE_NAME = "Group";
	private static final String ADDRRESS_STUDENT_LABLE_NAME = "Address";
	private static final String CITY_STUDENT_LABLE_NAME = "City";
	private static final String RESULT_STUDENT_LABLE_NAME = "Result";
	
	
	private static final int STUDENT_PHOTO_WIDTH = 180;
	private static final int STUDENT_PHOTO_HEIGHT = 220;
	
	
	private static final int TEXT_LIMIT = 130;
	private static final int RESULT_LIMIT = 1;

	
	private Label nameLabel;
	private Text nameText;
	
	private Label groupLabel;
	private Text groupText;
	
	
	private Label addressLabel;
	private Text addressText;
	
	private Label cityLabel;
	private Text cityText;
	
	private Label resultLabel;
	private Text resultText;
	
	
	
	private Label studentPhoto;

	private String imagePath;
	
	private DisableInputTextListener disableInputTextListener;
	
	public AbstractStudentViewComposite(Composite parent, int style) {
		super(parent, style);
		disableInputTextListener = new DisableInputTextListener();
	}

	
	private void initStudentPhoto(Composite composite) {
		
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.verticalAlignment = GridData.BEGINNING;
		gridData.horizontalSpan = 1;
		gridData.widthHint = STUDENT_PHOTO_WIDTH;
		gridData.heightHint = STUDENT_PHOTO_HEIGHT;
		gridData.verticalSpan = 5;
		gridData.horizontalIndent = 50;
		gridData.verticalIndent = 10;
		
		studentPhoto = new Label(this, SWT.BORDER | SWT.TOP);
		studentPhoto.setLayoutData(gridData);
	}

	
	
	
	private Image resize(Image image, int width, int height) {
		Image scaled = new Image(getDisplay(), width, height);
		GC gc = new GC(scaled);
		gc.setAntialias(SWT.ON);
		gc.setInterpolation(SWT.HIGH);
		gc.drawImage(image, 0, 0, 
		image.getBounds().width, image.getBounds().height, 0, 0, width, height);
		gc.dispose();
		
		image.dispose(); 
		
		return scaled;
	}
	
	protected void displayStudentPhoto(String imagePathFromDialog) {
		if ((imagePathFromDialog != null) && (!imagePathFromDialog.isEmpty())) {
			imagePath = imagePathFromDialog;
			Image image = new Image(getDisplay(), imagePath);
			studentPhoto.setImage(resize(image, STUDENT_PHOTO_WIDTH, STUDENT_PHOTO_HEIGHT));
		}
	}
	
	
	protected void initListenersForExistStudent () {
		
		
		groupText.addListener(SWT.Verify, disableInputTextListener);
		nameText.addListener(SWT.Verify, disableInputTextListener);
		addressText.addListener(SWT.Verify, disableInputTextListener);
		cityText.addListener(SWT.Verify, disableInputTextListener);
		resultText.addListener(SWT.Verify, disableInputTextListener);
	}
	
	
	protected void initListenersForNewStudent () {
		
		groupText.addListener(SWT.Verify, disableInputTextListener);
		resultText.addListener(SWT.Verify, new VerifyEditNumbersLisener());
	}
	
	protected void initStudentViewComposite() {
		GridData gridDataLabel, gridDataText;
		
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		gridLayout.marginRight = 20;
		gridLayout.marginBottom = 20;
//		gridLayout.horizontalSpacing = 50;
//		gridLayout.makeColumnsEqualWidth = true;
		setLayout(gridLayout);
		
		gridDataLabel = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        gridDataLabel.horizontalSpan = 1;
        gridDataLabel.verticalIndent = 15;
        gridDataLabel.horizontalIndent = 30;
        gridDataLabel.widthHint = 50;
        gridDataLabel.grabExcessHorizontalSpace = true;
		
        
        gridDataText = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
        gridDataText.horizontalSpan = 1;
        gridDataText.verticalIndent = 15;
        gridDataText.horizontalIndent = 15;
        gridDataText.widthHint = 200;
        gridDataText.grabExcessHorizontalSpace = true;
        
		nameLabel = new Label(this, SWT.NULL);
		nameLabel.setText(NAME_STUDENT_LABLE_NAME);
        nameLabel.setLayoutData(gridDataLabel);
        
        nameText = new Text(this, SWT.SINGLE | SWT.BORDER | SWT.FILL);
        nameText.setTextLimit(TEXT_LIMIT);
        nameText.setLayoutData(gridDataText);
        
        initStudentPhoto(this);
        
		groupLabel = new Label(this, SWT.NULL);
		groupLabel.setText(GROUP_STUDENT_LABLE_NAME);
		groupLabel.setLayoutData(gridDataLabel);
        
        groupText = new Text(this, SWT.SINGLE | SWT.BORDER | SWT.LEFT);
        groupText.setTextLimit(TEXT_LIMIT);
        groupText.setLayoutData(gridDataText);
        

		addressLabel = new Label(this, SWT.NULL);
		addressLabel.setText(ADDRRESS_STUDENT_LABLE_NAME);
		addressLabel.setLayoutData(gridDataLabel);
        
        addressText = new Text(this, SWT.SINGLE | SWT.BORDER | SWT.LEFT);
        addressText.setTextLimit(TEXT_LIMIT);
        addressText.setLayoutData(gridDataText);

		cityLabel = new Label(this, SWT.NULL);
		cityLabel.setText(CITY_STUDENT_LABLE_NAME);
		cityLabel.setLayoutData(gridDataLabel);
        
        cityText = new Text(this, SWT.SINGLE | SWT.BORDER | SWT.LEFT);
        cityText.setTextLimit(TEXT_LIMIT);
        cityText.setLayoutData(gridDataText);
        
		resultLabel = new Label(this, SWT.NULL);
		resultLabel.setText(RESULT_STUDENT_LABLE_NAME);
		resultLabel.setLayoutData(gridDataLabel);
        
        resultText = new Text(this, SWT.SINGLE | SWT.BORDER | SWT.LEFT);
        resultText.setTextLimit(RESULT_LIMIT);
        resultText.setLayoutData(gridDataText);
        
        
	}
	
	public void setFocusAtStartUp() {
		nameText.setFocus();
	}
	
	private boolean isPathExists(String path) {
		File file = new File(path);
		return file.exists();
	}
	
	private String getImagePathOrDefaultPath(String path) {
		if ((path != null) && (!path.isEmpty()) 
				&& (isPathExists(path))) {
			return path;
		} else {
			return RcpAppFileLocator.getProjectWorkspace() + ImageKeys.EMPTY_PHOTO.getStringValue();
		}
	}
	
	protected void setStudentToView(Optional<Student> student) {
		if (student.isPresent()) {
			nameText.setText(student.get().getName());
			groupText.setText(student.get().getGroup().get().getName());
			addressText.setText(student.get().getAddress());
			cityText.setText(student.get().getCity());
			resultText.setText(student.get().getResult().toString());
			String defaultImagePath = getImagePathOrDefaultPath(student.get().getImagePath());
			displayStudentPhoto(defaultImagePath);
		}
	}
	
	protected void setGroupNameForNewStudent(Optional<String> groupName) {
		if (groupName.isPresent()) {
			groupText.setText(groupName.get());
		}
	}
	
	public  boolean isDataValid() {
		boolean result = true;
		
		result &= StudentValidator.isNameValid(nameText.getText().trim());
		result &= StudentValidator.isAddressValid(addressText.getText().trim());
		result &= StudentValidator.isCityValid(cityText.getText().trim());
		result &= StudentValidator.isResultValid(resultText.getText().trim());
		
		return result;
	}
	
	
	public String getNotValidMessage() {
		StringBuilder result = new StringBuilder();
		
		if (!StudentValidator.isNameValid(nameText.getText().trim())) {
			result.append(StudentValidator.getValidationMessage());
		}
		if (!StudentValidator.isAddressValid(addressText.getText().trim())) {
			result.append(StudentValidator.getValidationMessage());
		}
		if (!StudentValidator.isCityValid(cityText.getText().trim())) {
			result.append(StudentValidator.getValidationMessage());
		}
		if (!StudentValidator.isResultValid(resultText.getText().trim())) {
			result.append(StudentValidator.getValidationMessage());
		}
		
		return result.toString();
	}
	
	public Student getStudentFromView() {
		Student student = new Student();
		student.setName(nameText.getText().trim());
		student.setAddress(addressText.getText().trim());
		student.setCity(cityText.getText().trim());
		student.setResult(Integer.valueOf(resultText.getText()));
		student.setImagePath(imagePath);
		
		return student;
		
	}


	@Override
	public void dispose() {
		studentPhoto.getImage().dispose();
		super.dispose();
	}

}
