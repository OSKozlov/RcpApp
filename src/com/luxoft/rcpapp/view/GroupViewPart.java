package com.luxoft.rcpapp.view;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.part.EditorInputTransfer;
import org.eclipse.ui.part.EditorInputTransfer.EditorInputData;
import org.eclipse.ui.part.ViewPart;

import com.luxoft.rcpapp.action.OpenStudentViewAction;
import com.luxoft.rcpapp.dao.GroupDao;
import com.luxoft.rcpapp.dao.StudentDao;
import com.luxoft.rcpapp.dto.StudentEditorInput;
import com.luxoft.rcpapp.dto.TreeObject;
import com.luxoft.rcpapp.dto.TreeParent;
import com.luxoft.rcpapp.listener.GroupViewCellModifier;
import com.luxoft.rcpapp.model.Student;
import com.luxoft.rcpapp.provider.GroupViewContentProvider;
import com.luxoft.rcpapp.provider.GroupViewLabelProvider;
import com.luxoft.rcpapp.util.TreeViewConverter;

public class GroupViewPart extends ViewPart {
	
	public static final String ID = "com.luxoft.rcpapp.views.group";
	public static final String ROOT_FOLDER_NAME = "Folder";

	@Inject
	private StudentDao studentDao;
	
	@Inject
	private GroupDao groupDao;
	
	@Inject 
	private TreeViewConverter converter;
	
	private TreeViewer treeViewer;

	private GroupViewCellModifier groupViewCellModifier;
	
	private static Logger logger = Logger.getLogger(GroupViewPart.class.getName());
	
	public GroupViewPart() {
		super();
	}

	private Optional<IEditorInput> getEditorInput() {
		Optional<IEditorInput> editorInput = Optional.empty();
		
		ISelection selection =  treeViewer.getSelection();
		if ((selection != null) && (selection instanceof IStructuredSelection)) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if ((structuredSelection.size() == 1) 
					&& (structuredSelection.getFirstElement().getClass() == TreeObject.class)) {
				
				TreeObject treeObject = (TreeObject) structuredSelection.getFirstElement();
				if ((treeObject != null) && (checkInjectedObjects())) {
					Optional<Student> student = studentDao.getByID(treeObject.getId());
					if (student.isPresent()) {
						StudentEditorInput studentEditorInput = new StudentEditorInput(student.get());
						editorInput = Optional.of(studentEditorInput);
					}
				}
			}
		}
		
		return editorInput;

	}
	
	private void initDragAndDrop (final StructuredViewer viewer) {
		int operations = DND.DROP_COPY;
		
		
		Transfer [] transferTypes = new Transfer [] { EditorInputTransfer.getInstance() };
		DragSourceListener listener = new DragSourceAdapter() {

			@Override
			public void dragSetData(DragSourceEvent event) {
				boolean isDoit = false;
				
				if (EditorInputTransfer.getInstance().isSupportedType(event.dataType)) {
					Optional<IEditorInput> editorInput = getEditorInput();
					if (editorInput.isPresent()) {
						EditorInputData[] inputs = new EditorInputData[1];
						inputs[0] = EditorInputTransfer.createEditorInputData(StudentEditorPart.ID, editorInput.get()); 
						event.data = inputs;
						isDoit = true;
					}
				}
				event.doit = isDoit;
			}

			@Override
			public void dragStart(DragSourceEvent event) {
				super.dragStart(event);
			}

			@Override
			public void dragFinished(DragSourceEvent event) {
			}
			
			
		};
		
		viewer.addDragSupport(operations, transferTypes, listener);
	}
	
	private void initContextMenu() {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		getSite().registerContextMenu(menuMgr, treeViewer);
			
		Control control = treeViewer.getControl();
		Menu menu = menuMgr.createContextMenu(control);
		control.setMenu(menu);
	}
	
	private boolean checkInjectedObjects() {
		
		if((groupDao == null) || (studentDao == null)) {
			logger.log(Level.WARNING, "An error occurred to inject objects", "An error occurred to inject objects");
			
			MessageDialog.openError(getSite().getShell(), "Error", 
					"An error occurred to inject objects");
			return false;
		}
		
		return true;
	}
	
	private TreeObject initDataModel() {
		TreeParent treeParent = converter.convertGroupsToDto(groupDao.load(Optional.empty()));
		return converter.convertStudentsToDto(treeParent, studentDao.load(Optional.empty()));
	}
	
	@Override
	public void createPartControl(Composite parent) {
		treeViewer = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		treeViewer.setContentProvider(new GroupViewContentProvider());
		treeViewer.setLabelProvider(new GroupViewLabelProvider());
		if (checkInjectedObjects()) {
			treeViewer.setInput(initDataModel());
			treeViewer.expandAll();
		}
		
		treeViewer.addDoubleClickListener(new IDoubleClickListener() {
			
			@Override
			public void doubleClick(DoubleClickEvent event) {
				OpenStudentViewAction action = getSite().getService(IEclipseContext.class).get(OpenStudentViewAction.class);
				action.run();
			}
		});

		treeViewer.setCellEditors(new CellEditor[] {
				new TextCellEditor(treeViewer.getTree())
		});
		
		treeViewer.setColumnProperties(new String[] { "name" });
		groupViewCellModifier = new GroupViewCellModifier(getSite().getWorkbenchWindow());
		treeViewer.setCellModifier(groupViewCellModifier);
		ContextInjectionFactory.inject(groupViewCellModifier, getSite().getService(IEclipseContext.class));
		

		TreeViewerEditor.create(treeViewer, new ColumnViewerEditorActivationStrategy(treeViewer) {

			@Override
			protected boolean isEditorActivationEvent(ColumnViewerEditorActivationEvent event) {
//				System.err.println("### isEditorActivationEvent: " + event.eventType);
				return event.eventType == 
						ColumnViewerEditorActivationEvent.PROGRAMMATIC;
			}
			
		}, ColumnViewerEditor.DEFAULT);
		
		initDragAndDrop(treeViewer);
		initContextMenu();
		getSite().setSelectionProvider(treeViewer);
	}
	
	public void editTreeViewElement() {
//		System.err.println("### editTreeViewElement begin");
		IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
		if (selection != null) {
			treeViewer.editElement(selection.getFirstElement(), 0);
//			System.err.println("### editTreeViewElement end");
		}
	}
	
	
	@Override
	public void setFocus() {
		treeViewer.getControl().setFocus();

	}
	
	
	public void reloadDataModel(TreeObject object) {
		treeViewer.setInput(object);
    	treeViewer.refresh();
    	treeViewer.expandAll();
	}
	
	private void deleteChildTreeObject(TreeObject deletedObject, TreeObject [] treeObjects) {
		for (TreeObject treeObject : treeObjects) {
			if (treeObject instanceof TreeParent) {
				TreeParent parent = (TreeParent) treeObject;
				if (parent.removeChildren(deletedObject)) {
					return;
				} else {
					deleteChildTreeObject(deletedObject, parent.getChildren());
				}
			} 
		}
	}
	
	
	public void deleteTreeObject(TreeObject object) {
		TreeObject currentObject = (TreeObject) treeViewer.getInput();
		if (currentObject instanceof TreeParent) {
			deleteChildTreeObject(object, ((TreeParent) currentObject).getChildren());
		}
		
		treeViewer.refresh();
	}

	private void addToTreeObject(TreeObject [] treeObjects, TreeObject newObject, TreeParent parent) {
		for (TreeObject treeObject : treeObjects) {
			if (treeObject instanceof TreeParent) {
				TreeParent currentParent = (TreeParent) treeObject;
				if (currentParent.getId().equals(parent.getId())) {
					currentParent.addChild(newObject);
					return;
				} else {
					addToTreeObject(currentParent.getChildren(), newObject, parent);
				}
			}
		}
	}
	
	
	public void addStudentToGroupView(Student student) {
		TreeObject currentObject = (TreeObject) treeViewer.getInput();
		TreeParent parent = converter.convertToTreeParent(student.getGroup().get());
		TreeObject newTreeObject = converter.convertToTreeObject(student);
		if (currentObject instanceof TreeParent) {
			addToTreeObject(((TreeParent) currentObject).getChildren(), newTreeObject, parent);
		}
		
		treeViewer.refresh();
	}

	@Override
	public void dispose() {
		ContextInjectionFactory.uninject(groupViewCellModifier, getSite().getService(IEclipseContext.class));
		super.dispose();
	}
	
	
	
}
