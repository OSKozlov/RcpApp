package com.luxoft.rcpapp.view;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;

import com.luxoft.rcpapp.model.Student;

public class ExistStudentViewComposite extends AbstractStudentViewComposite {
	
	
	public ExistStudentViewComposite(Composite parent, int style) {
		super(parent, style);
	}

	
	public void init(Optional<Student> student) {
		initStudentViewComposite();
		setStudentToView(student);
		initListeners();
	}
	
	private void initListeners () {
		initListenersForExistStudent();
	}
	
	
}
