package com.luxoft.rcpapp.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.PlatformUI;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.luxoft.rcpapp.model.GroupOfStudents;
import com.luxoft.rcpapp.model.Student;
import com.luxoft.rcpapp.util.RcpAppFileLocator;


public class StudentDao implements IDao<Student> {
	
	private final String ERROR_MESSAGE_TITLE = "An error occurred during input/output students list in/from file";
	
	private final String DEFAULT_PATH = "/student.json";
	private static Integer maxID = 0;
	
	private static Logger logger = Logger.getLogger(StudentDao.class.getName());
	
	private List<Student> students;
	
	

	public StudentDao() {
		students = new ArrayList<>();
	}
	
	public List<Student> createNewStudents() {
		
		students.clear();
		
		GroupOfStudents group = new GroupOfStudents(1, "NGroup 1");
		
		students.add(new Student(1, "NIvan Ivanov", "qwerty", "Dnepr", 5, group));
		students.add(new Student(2, "NPetr Petrov", "qwerty2", "Dnepr", 4, group));
		
		group = new GroupOfStudents(2, "NGroup 2");
		
		students.add(new Student(3, "NAlex Alexeev", "qwerty3", "Dnepr", 3, group));
		maxID = 3;
		
		return students;
	}
	
	private String definePathToFile (Optional<String> pathToFile) {
		String path;
		
		if (pathToFile.isPresent()) {
			path = pathToFile.get().trim();
		} else {
			path = RcpAppFileLocator.getProjectWorkspace();
		}
		
		return path + DEFAULT_PATH;
	}
	
	private void defineMaxId() {
		maxID =  students.stream()
				.mapToInt(student->student.getId())
				.max()
				.orElse(0);
	}
	
	@Override
	public List<Student> load(Optional<String> pathToFile) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Jdk8Module());
		
		students.clear();
		
		try {
			students = objectMapper.readValue(new File(definePathToFile(pathToFile)), new TypeReference<List<Student>>() {});
			defineMaxId();
		} catch (Exception e) {
			logger.log(Level.WARNING, ERROR_MESSAGE_TITLE, e);

			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), ERROR_MESSAGE_TITLE,
					e.getMessage());
		}
		
		return students;
	
	}
	
	

	@Override
	public List<Student> getAll() {
		return students;
	}

	@Override
	public boolean saveAll(Optional<String> pathToFile) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Jdk8Module());
		
		try {
			
			objectMapper.writeValue(new File(definePathToFile(pathToFile)), students);
			return true;
		} catch (Exception e) {
			logger.log(Level.WARNING, ERROR_MESSAGE_TITLE, e);
			
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
					ERROR_MESSAGE_TITLE, e.getMessage());
			
			return false;
		}
	}


	@Override
	public Optional<Student> getByID(Integer id) {
		List<Student> findedStudents =  students.stream()
		.filter(student -> student.getId().equals(id))
		.collect(Collectors.toList());
		
		if (findedStudents.size() > 0) {
			return Optional.of(findedStudents.get(0));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public boolean deleteById(Integer id) {
		Optional<Student> deletedStudent = students.stream()
				.filter(student -> student.getId().equals(id))
				.findFirst();
		
		if (deletedStudent.isPresent()) {
			return students.remove(deletedStudent.get());
		}
		
		return false;
	}

	@Override
	public Student addNew(Student object) {
		if (!students.contains(object)) {
			object.setId(++maxID);
			students.add(object);
		}
		
		return object;
	}

}
