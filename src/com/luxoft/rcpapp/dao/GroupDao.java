package com.luxoft.rcpapp.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.PlatformUI;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.luxoft.rcpapp.model.GroupOfStudents;
import com.luxoft.rcpapp.util.RcpAppFileLocator;


public class GroupDao implements IDao<GroupOfStudents>{

	private final String ERROR_MESSAGE_TITLE = "An error occurred during input/output groups list in/from file";
	
	private final String DEFAULT_PATH = "/group.json";
	private static Integer maxID = 0;
	
	private static Logger logger = Logger.getLogger(GroupDao.class.getName());
	
	private List<GroupOfStudents> groups;
	
	
	public GroupDao() {
		groups = new ArrayList<>();
	}
	
	@Override
	public List<GroupOfStudents> getAll() {
		return groups;
	}

	private String definePathToFile (Optional<String> pathToFile) {
		String path;
		
		if (pathToFile.isPresent()) {
			path = pathToFile.get().trim();
		} else {
			path = RcpAppFileLocator.getProjectWorkspace();
		}
		
		return path + DEFAULT_PATH;
	}
	
	private void defineMaxId() {
		maxID =  groups.stream()
				.mapToInt(student->student.getId())
				.max()
				.orElse(0);
	}
	
	@Override
	public List<GroupOfStudents> load(Optional<String> pathToFile) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Jdk8Module());
		
		groups.clear();
		
		try {
			groups = objectMapper.readValue(new File(definePathToFile(pathToFile)), new TypeReference<List<GroupOfStudents>>() {});
			defineMaxId();
/*			groups.add(new GroupOfStudents(1, "Group 1"));
			groups.add(new GroupOfStudents(2, "Group 2"));
			groups.add(new GroupOfStudents(3, "Group 3"));
*/			
		} catch (Exception e) {
			logger.log(Level.WARNING, ERROR_MESSAGE_TITLE, e);

			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), ERROR_MESSAGE_TITLE,
					e.getMessage());
		}
		
		return groups;
	}

	@Override
	public boolean saveAll(Optional<String> pathToFile) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Jdk8Module());
		
		try {
			
			objectMapper.writeValue(new File(definePathToFile(pathToFile)), groups);
			return true;
		} catch (Exception e) {
			logger.log(Level.WARNING, ERROR_MESSAGE_TITLE, e);
			
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), 
					ERROR_MESSAGE_TITLE, e.getMessage());
			
			return false;
		}
		
	}

	@Override
	public Optional<GroupOfStudents> getByID(Integer id) {
		List<GroupOfStudents> findedGroups =  groups.stream()
		.filter(group -> group.getId().equals(id))
		.collect(Collectors.toList());
		
		if (findedGroups.size() > 0) {
			return Optional.of(findedGroups.get(0));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public boolean deleteById(Integer id) {
		Optional<GroupOfStudents> deletedStudent = groups.stream()
				.filter(group -> group.getId().equals(id))
				.findFirst();
		
		if (deletedStudent.isPresent()) {
			return groups.remove(deletedStudent.get());
		}
		
		return false;
	}

	@Override
	public GroupOfStudents addNew(GroupOfStudents object) {
		if (!groups.contains(object)) {
			object.setId(++maxID);
			groups.add(object);
		}
		
		return object;
	}

}
