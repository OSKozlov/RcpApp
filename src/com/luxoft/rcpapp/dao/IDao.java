package com.luxoft.rcpapp.dao;

import java.util.List;
import java.util.Optional;

public interface IDao<T> {

	List<T> getAll();
	
	List<T> load(Optional<String> pathToFile);
	
	boolean saveAll (Optional<String> pathToFile);
	
	Optional<T> getByID(Integer id);
	
	boolean deleteById(Integer id);

	T addNew (T object);
	
}
