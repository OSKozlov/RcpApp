package com.luxoft.rcpapp.adapter;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorInputTransfer;

public class EditorAreaDropAdapter extends DropTargetAdapter {
	
	private final IWorkbenchWindow window;
	
	private static Logger logger = Logger.getLogger(EditorAreaDropAdapter.class.getName());

	public EditorAreaDropAdapter(IWorkbenchWindow window) {
		super();
		this.window = window;
	}
	
	
	@Override
	public void dragEnter(DropTargetEvent event) {
		event.detail = DND.DROP_COPY;
	}


	@Override
	public void dragOperationChanged(DropTargetEvent event) {
		event.detail = DND.DROP_COPY;
	}


	@Override
	public void drop(DropTargetEvent event) {
		Display display = window.getShell().getDisplay();
		final IWorkbenchPage page = window.getActivePage();
		if (page != null) {
			display.asyncExec(() -> handleDrop(event, page));
		}
	}


	private void handleDrop(DropTargetEvent event, IWorkbenchPage page) {

		if (EditorInputTransfer.getInstance().isSupportedType(
				event.currentDataType)) {
			
			
			EditorInputTransfer.EditorInputData[] editorInputs =
					(EditorInputTransfer.EditorInputData[]) event.data;
			if (editorInputs == null) {
				return;
			}
			for (int i = 0; i < editorInputs.length; i++) {
				IEditorInput editorInput = editorInputs[i].input;
				String editorId = editorInputs[i].editorId;
				openEditor(page, editorInput, editorId);
			}
		}
	}
	
	private IEditorPart openEditor(IWorkbenchPage page, 
			IEditorInput editorInput, String editorId) {
		
		IEditorPart part = null;
		
		if ((editorId != null) && (!editorId.isEmpty())) {
			try {
				part = page.openEditor(editorInput, editorId);
			} catch (PartInitException e) {
				logger.log(Level.WARNING, "An error occurred during open new editor part", e);
				
				MessageDialog.openError(window.getShell(), "Error", 
						"An error occurred during open new editor part");
			}
		}
		
		return part;
	}

}
