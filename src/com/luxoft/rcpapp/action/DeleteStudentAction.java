package com.luxoft.rcpapp.action;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.luxoft.rcpapp.application.Application;
import com.luxoft.rcpapp.dao.StudentDao;
import com.luxoft.rcpapp.dto.TreeObject;
import com.luxoft.rcpapp.view.GroupViewPart;
import com.luxoft.rcpapp.view.ImageKeys;

public class DeleteStudentAction extends Action implements ISelectionListener, IWorkbenchAction {
	
	public final String ID = "com.luxoft.rcpapp.action.deleteStudent";
	
	private final IWorkbenchWindow window;
	private IStructuredSelection selection;
	private GroupViewPart groupViewPart;
	
	private static Logger logger = Logger.getLogger(DeleteStudentAction.class.getName());
	
	
	@Inject 
	private StudentDao studentdao;
	
	public DeleteStudentAction(IWorkbenchWindow window) {
		this.window = window;
		setId(ID);
		setActionDefinitionId(ID);
		setText("&Delete student...");
		setToolTipText("Delete student from group view list");
		
		setImageDescriptor(
				AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, ImageKeys.DELETE_STUDENT.getStringValue()));
		
		window.getSelectionService().addSelectionListener(this);
	}



	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection newSelection) {
		boolean isActionEnabled = false;
		
		if (part instanceof GroupViewPart) {
			if (groupViewPart == null) {
				groupViewPart = (GroupViewPart) part;
			}
			
			if (newSelection instanceof IStructuredSelection) {
				selection = (IStructuredSelection) newSelection;
				if ((selection.size() == 1) && (selection.getFirstElement().getClass() == TreeObject.class)) {
						isActionEnabled = true;
				}
			} 
		}
		
		setEnabled(isActionEnabled);
	}



	@Override
	public void run() {
		if ((selection != null) && (selection.size() == 1) 
				&& (selection.getFirstElement() instanceof TreeObject)) {
			
			boolean canDelete = MessageDialog.openQuestion(window.getShell(), "Deleting curent ", 
					"Are you shure want delete current student?");
			
			if (canDelete) {
				
				TreeObject deletedObject = (TreeObject) selection.getFirstElement();
				if (studentdao != null) {
					studentdao.deleteById(deletedObject.getId());
					groupViewPart.deleteTreeObject(deletedObject);
				} else {
					logger.log(Level.WARNING, "An error occurred to inject objects", "An error occurred to inject objects");
					
					MessageDialog.openError(window.getShell(), "Error", 
							"An error occurred to inject objects");
				}
				
			}
		}
		
	}


	@Override
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
		
	}
	
	

}
