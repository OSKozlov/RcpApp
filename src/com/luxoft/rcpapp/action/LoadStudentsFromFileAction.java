package com.luxoft.rcpapp.action;

import java.util.Optional;

import javax.inject.Inject;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.luxoft.rcpapp.application.Application;
import com.luxoft.rcpapp.dao.GroupDao;
import com.luxoft.rcpapp.dao.StudentDao;
import com.luxoft.rcpapp.dto.TreeParent;
import com.luxoft.rcpapp.util.RcpAppFileLocator;
import com.luxoft.rcpapp.util.TreeViewConverter;
import com.luxoft.rcpapp.view.GroupViewPart;
import com.luxoft.rcpapp.view.ImageKeys;

public class LoadStudentsFromFileAction extends Action {
	public final String ID = "com.luxoft.rcpapp.action.loadStudentsFromFile";
	
	private final IWorkbenchWindow window;
	
	@Inject 
	private GroupDao groupDao;
	
	@Inject
	private StudentDao studentDao;
	
	@Inject
	private TreeViewConverter converter;
	
	public LoadStudentsFromFileAction(IWorkbenchWindow window) {
		this.window = window;
		
		setId(ID);
		setActionDefinitionId(ID);
		setText("&Load students...");
		setToolTipText("Load students from external file");
		
		setImageDescriptor(
				AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, ImageKeys.LOAD_FROM_FILE.getStringValue()));
	}

	
	private Optional<String> getSelectedPath() {
		DirectoryDialog dd = new DirectoryDialog(window.getShell());
        dd.setMessage("Please chose a directory for the load from files");
        dd.setFilterPath(RcpAppFileLocator.getProjectWorkspace());
        return Optional.ofNullable(dd.open());
        
	}

	
	private Optional<GroupViewPart> getGroupViewPart() {
		IViewReference [] references = window.getActivePage().getViewReferences();
		for (IViewReference iViewReference : references) {
				if (iViewReference.getId().equals(GroupViewPart.ID)) {
						return Optional.ofNullable(((GroupViewPart) iViewReference.getView(true)));
				}
		}
		
		return Optional.empty();

	}
	
	@Override
	public void run() {
		if (window != null) {
			Optional<String> selectedPath = getSelectedPath(); 
			if (selectedPath.isPresent()) {
				Optional<GroupViewPart> part = getGroupViewPart();
				if (part.isPresent()) {
					TreeParent treeParent = converter.convertGroupsToDto(groupDao.load(selectedPath));
					part.get().reloadDataModel(converter.convertStudentsToDto(treeParent, studentDao.load(selectedPath)));
				}
			}
		}
		
	}


}
