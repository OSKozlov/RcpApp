package com.luxoft.rcpapp.action;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.luxoft.rcpapp.application.Application;
import com.luxoft.rcpapp.dao.GroupDao;
import com.luxoft.rcpapp.dao.StudentDao;
import com.luxoft.rcpapp.util.RcpAppFileLocator;
import com.luxoft.rcpapp.view.ImageKeys;

public class SaveStudentsToFileAction extends Action {
	public final String ID = "com.luxoft.rcpapp.action.saveStudentsToFile";
	
	private static Logger logger = Logger.getLogger(SaveStudentsToFileAction.class.getName());
	
	private final IWorkbenchWindow window;
	
	@Inject 
	private GroupDao groupDao;
	
	@Inject
	private StudentDao studentDao;
	
	public SaveStudentsToFileAction(IWorkbenchWindow window) {
		this.window = window;
		
		setId(ID);
		setActionDefinitionId(ID);
		setText("&Save students...");
		setToolTipText("Save students to external file");
		
		setImageDescriptor(
				AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, ImageKeys.SAVE_TO_FILE.getStringValue()));
	}

	
	
	@Override
	public void run() {
		if (window != null) {
			DirectoryDialog dd = new DirectoryDialog(window.getShell());
	        dd.setMessage("Please chose a directory for the save to files");
	        dd.setFilterPath(RcpAppFileLocator.getProjectWorkspace());
	        String selectedPath = dd.open();
	        if (selectedPath != null) {
	        	if ((groupDao != null) && (studentDao != null)) { 
		        	if (groupDao.saveAll(Optional.of(selectedPath)) &&  
		        	studentDao.saveAll(Optional.of(selectedPath))) {
		        		MessageDialog.openInformation(window.getShell(), "Save to files", "All dala saved");
		        	}
	        	} else {
	        		logger.log(Level.WARNING, "An error occurred to inject objects", "An error occurred to inject objects");
	    			
	    			MessageDialog.openError(window.getShell(), "Error", 
	    					"An error occurred to inject objects");
	        	}
	        }
		}
		
	}


}
