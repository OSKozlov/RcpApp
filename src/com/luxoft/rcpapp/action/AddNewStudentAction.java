package com.luxoft.rcpapp.action;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.luxoft.rcpapp.application.Application;
import com.luxoft.rcpapp.dao.GroupDao;
import com.luxoft.rcpapp.dao.StudentDao;
import com.luxoft.rcpapp.dto.TreeParent;
import com.luxoft.rcpapp.model.GroupOfStudents;
import com.luxoft.rcpapp.model.Student;
import com.luxoft.rcpapp.view.GroupViewPart;
import com.luxoft.rcpapp.view.ImageKeys;
import com.luxoft.rcpapp.view.NewStudentDialog;

public class AddNewStudentAction extends Action implements ISelectionListener, IWorkbenchAction {
	
	public final String ID = "com.luxoft.rcpapp.action.addNewStudent";
	
	private final IWorkbenchWindow window;
	private IStructuredSelection selection;
	
	private GroupViewPart groupViewPart;
	
	@Inject 
	private GroupDao groupDao;
	
	@Inject
	private StudentDao studentDao;
	
	private static Logger logger = Logger.getLogger(AddNewStudentAction.class.getName());
	
	public AddNewStudentAction(IWorkbenchWindow window) {
		this.window = window;
		setId(ID);
		setActionDefinitionId(ID);
		setText("&Add new student...");
		setToolTipText("Add new student to your group view list");
		
		setImageDescriptor(
				AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, ImageKeys.ADD_STUDENT.getStringValue()));
		
		setEnabled(false);
		window.getSelectionService().addSelectionListener(this);
	}



	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection newSelection) {
		boolean isActionEnabled = false;
		
		if (part instanceof GroupViewPart) {
			if (groupViewPart == null) {
				groupViewPart = (GroupViewPart) part;
			}

			if (newSelection instanceof IStructuredSelection) {
				selection = (IStructuredSelection) newSelection;
				if ((selection.size() == 1) && (selection.getFirstElement() instanceof TreeParent)) {
					TreeParent parent = (TreeParent) selection.getFirstElement(); 
					if ((parent != null) && (!parent.getName().equals(GroupViewPart.ROOT_FOLDER_NAME))) {
						isActionEnabled = true;
					}
				}
			} 
		}
		
		setEnabled(isActionEnabled);
	}



	@Override
	public void run() {
		if (isEnabled()) {
			NewStudentDialog dialog = new NewStudentDialog(window.getShell());
			TreeParent parent = (TreeParent) selection.getFirstElement();
			if (parent != null) {
				dialog.setStudentGroup(parent.getName());
				if (dialog.open() == Window.OK) {
					Student newStudent = dialog.getNewStudent();
					Optional<GroupOfStudents> group = groupDao.getByID(parent.getId());
					if (group.isPresent() && (studentDao != null)) {
						newStudent.setGroup(group.get());
						studentDao.addNew(newStudent);
						groupViewPart.addStudentToGroupView(newStudent);
					} else {
						logger.log(Level.WARNING, "An error occurred during get new student from dialog", "Can't find group for student");
						MessageDialog.openError(window.getShell(), "Error", "Group of students" + parent.getName() + " not found!");
					}
				}
			}
		}
	}



	@Override
	public void dispose() {
		window.getSelectionService().removeSelectionListener(this);
		
	}
	
	

}
