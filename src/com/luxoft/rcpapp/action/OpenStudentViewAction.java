package com.luxoft.rcpapp.action;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.luxoft.rcpapp.application.Application;
import com.luxoft.rcpapp.dao.StudentDao;
import com.luxoft.rcpapp.dto.StudentEditorInput;
import com.luxoft.rcpapp.dto.TreeObject;
import com.luxoft.rcpapp.model.Student;
import com.luxoft.rcpapp.view.GroupViewPart;
import com.luxoft.rcpapp.view.ImageKeys;
import com.luxoft.rcpapp.view.StudentEditorPart;

public class OpenStudentViewAction extends Action {
	
	public final String ID = "com.luxoft.rcpapp.action.openStudentView";
	
	private final IWorkbenchWindow window;
	
	private static Logger logger = Logger.getLogger(OpenStudentViewAction.class.getName());
	
	@Inject
	private StudentDao studentDao;

	public OpenStudentViewAction(IWorkbenchWindow window) {
		this.window = window;
		
		setId(ID);
		setActionDefinitionId(ID);
		setText("&Open student info...");
		setToolTipText("Open information about student");
		
		setImageDescriptor(
				AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, ImageKeys.STUDENT_32.getStringValue()));
		
	}

	
	private void openNewFolder(TreeObject treeObject) {
		if (studentDao != null) {
			Optional<Student> student = studentDao.getByID(treeObject.getId());
		
			if (student.isPresent()) {
				StudentEditorInput editorInput = new StudentEditorInput(student.get());
				IWorkbenchPage iWorkbenchPage = window.getActivePage();
				IEditorPart iEditorPart = iWorkbenchPage.findEditor(editorInput);
				if (iEditorPart != null) {
					iWorkbenchPage.activate(iEditorPart);
				} else {
					try {
						iWorkbenchPage.openEditor(editorInput, StudentEditorPart.ID);
					} catch (PartInitException e) {
						logger.log(Level.WARNING, "An error occurred during open new editor part", e);
						
						MessageDialog.openError(window.getShell(), "Error", 
								"An error occurred during open new editor part");
		
					}
				}
			} else {
				logger.log(Level.WARNING, "An error occurred during get student from tree view", 
						"Can't find student by id = " + treeObject.getId());
				
				MessageDialog.openError(window.getShell(), "Error", 
						"Group student by name" + treeObject.getName() + " not found!");
	
			}
		} else {
			logger.log(Level.WARNING, "An error occurred to inject objects", "An error occurred to inject objects");
			
			MessageDialog.openError(window.getShell(), "Error", 
					"An error occurred to inject objects");
		}
	}
	
	@Override
	public void run() {
		if (window != null) {
			ISelection selection =  window.getSelectionService().getSelection(GroupViewPart.ID);
			if ((selection != null) && (selection instanceof IStructuredSelection)) {
				IStructuredSelection structuredSelection = (IStructuredSelection) selection;
				if ((structuredSelection.size() == 1) 
						&& (structuredSelection.getFirstElement().getClass() == TreeObject.class)) {
					
					TreeObject treeObject = (TreeObject) structuredSelection.getFirstElement();
					openNewFolder(treeObject);
				}
			}
		}
				
	}

	
	

}
