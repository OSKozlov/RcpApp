package com.luxoft.rcpapp.action;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;

import com.luxoft.rcpapp.view.GroupViewPart;

public class RenameTreeObjectNameAction extends Action {
	
	public final String ID = "com.luxoft.rcpapp.action.renameTreeObjectName";
	
	private final IWorkbenchWindow window;
	
	public RenameTreeObjectNameAction(IWorkbenchWindow window) {
		this.window = window;
		
		setId(ID);
		setActionDefinitionId(ID);
		setText("&Rename...");
		setToolTipText("Rename name");
		
	}

	
	@Override
	public void run() {
		if (window != null) {
			IWorkbenchPart part = window.getPartService().getActivePart(); 
			if ( part instanceof GroupViewPart) {
				((GroupViewPart) part).editTreeViewElement();
				
			}
		}
				
	}

	
	

}
