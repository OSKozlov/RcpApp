package com.luxoft.rcpapp.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.RadioState;

public class OptionHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		if(HandlerUtil.matchesRadioState(event)) { 
	        return null;
		}
		
		String currentState = event.getParameter(RadioState.PARAMETER_ID);
//	    System.err.println("### AlignHandler currentState: " + currentState );
	    // perform task for current state 
	    if(currentState.equals("option_1")) {
	    // perform left alignment 
	    } else if(currentState.equals("option_2")) { 
	    // perform center alignment 
	    // and so on ... 
	    }
	    
	    IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
	    
	    MessageDialog.openInformation(window.getShell(), "Option pressed..", currentState);
	    // and finally update the current state 
	    HandlerUtil.updateRadioState(event.getCommand(), currentState);
	    
		return null;
	}

	
}
