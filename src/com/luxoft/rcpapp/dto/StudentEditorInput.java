package com.luxoft.rcpapp.dto;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPersistableElement;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.luxoft.rcpapp.model.Student;

public class StudentEditorInput implements IEditorInput, IPersistableElement {
	
	public final static String KEY_NAME = "student.json";
	
	private static Logger logger = Logger.getLogger(StudentEditorInput.class.getName());
	
	private Student student;

	
	public StudentEditorInput(Student student) {
		super();
		this.student = student;
	}

	@Override
	public <T> T getAdapter(Class<T> adapter) {
		return null;
	}

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	@Override
	public String getName() {
		if (student != null) {
			return student.getName();
		}
		
		return "";
	}

	@Override
	public IPersistableElement getPersistable() {
		return this;
	}

	@Override
	public String getToolTipText() {
		if (student != null) {
			return student.getName();
		}
		return "";
	}

	public Student getStudent() {
		return student;
	}

	
	@Override
	public void saveState(IMemento memento) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Jdk8Module());
		
		try {
			
			String jsonStudent = objectMapper.writeValueAsString(student);
			memento.putString(KEY_NAME, jsonStudent);
			
		} catch (JsonProcessingException e) {
			logger.log(Level.WARNING, "An error occurred during create Student object for DND", e);
			
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Error", 
					"An error occurred during create Student object for DND");
		}
		
		
	}

	@Override
	public String getFactoryId() {
		return StudentEditorInputFactory.ID;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((student == null) ? 0 : student.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		StudentEditorInput other = (StudentEditorInput) obj;
		if (student == null) {
			if (other.student != null) {
				return false;
			}
		} else if (!student.equals(other.student)) {
			return false;
		}
		return true;
	}

}
