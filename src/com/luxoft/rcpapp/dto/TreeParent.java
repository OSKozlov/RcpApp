package com.luxoft.rcpapp.dto;

import java.util.ArrayList;
import java.util.List;

public class TreeParent extends TreeObject {
	private List<TreeObject> children;
	
	public TreeParent(Integer id, String name) {
		super(id, name);
		children = new ArrayList<>();
	}
	
	
	public void addChild(TreeObject child) {
		children.add(child);
		child.setParent(this);
	}
	
	
	public void removeChild(TreeObject child) {
		children.remove(child);
		child.setParent(null);
	}
	
	
	public boolean addChildren(List<TreeObject> treeObjects) {
		if ((treeObjects != null) && (treeObjects.size() > 0)) {
			treeObjects.stream().forEach(treeObject->setParent(this));
			children = treeObjects;
			return true;
		} else {
			return false;
		}
	}
	
	public TreeObject[] getChildren() {
		return (TreeObject[]) children.toArray(new TreeObject[children.size()]);
	}
	
	public boolean removeChildren(TreeObject deletedObject) {
		return children.remove(deletedObject);
	}
	
	public boolean hasChildren() {
		return children.size() > 0;
	}

}
