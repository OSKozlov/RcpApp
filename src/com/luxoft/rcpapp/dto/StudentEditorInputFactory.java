package com.luxoft.rcpapp.dto;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.luxoft.rcpapp.model.Student;

public class StudentEditorInputFactory implements IElementFactory {

	public static final String ID = "com.luxoft.rcpapp.dto.studentEditorInputFactory";
	
	private static Logger logger = Logger.getLogger(StudentEditorInputFactory.class.getName());
	
	@Override
	public IAdaptable createElement(IMemento memento) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Jdk8Module());
		
		String jsonStudent = memento.getString(StudentEditorInput.KEY_NAME);
		
		if ((jsonStudent != null) && (!jsonStudent.isEmpty()) ) {
			try {
				Student student = objectMapper.readValue(jsonStudent, Student.class);
				if (student != null) {
					return new StudentEditorInput(student); 
					}
			} catch (IOException e) {
				logger.log(Level.WARNING, "An error occurred during create Student object for DND", e);
				
				MessageDialog.openError(Display.getCurrent().getActiveShell(), "Error", 
						"An error occurred during create Student object for DND");
			}
			
		}
		return null;
	}

}
