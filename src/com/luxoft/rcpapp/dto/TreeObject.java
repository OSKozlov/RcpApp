package com.luxoft.rcpapp.dto;

import java.util.Optional;

public class TreeObject {
	private Integer id;
	private String name;
	private TreeParent parent;

	public TreeObject(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setParent(TreeParent parent) {
		this.parent = parent;
	}
	
	public Optional<TreeParent> getParent() {
		return Optional.ofNullable(parent);
	}
	
	public String toString() {
		return name;
	}
	
	
}
