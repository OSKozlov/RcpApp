package com.luxoft.rcpapp.provider;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.luxoft.rcpapp.application.Application;
import com.luxoft.rcpapp.dto.TreeParent;
import com.luxoft.rcpapp.view.GroupViewPart;
import com.luxoft.rcpapp.view.ImageKeys;

public class GroupViewLabelProvider extends LabelProvider {
	
	private static final String GROUP = "GROUP";
	private static final String STUDENT = "STUDENT";
	private static final String ROOT = "ROOT";

	
	public GroupViewLabelProvider() {
		 
		if (JFaceResources.getImageRegistry().getDescriptor(GROUP) == null) {
			JFaceResources.getImageRegistry().put(GROUP, getImage(ImageKeys.GROUP_OF_STUDENTS.getStringValue()));
		}
		
		if (JFaceResources.getImageRegistry().getDescriptor(ROOT) == null) {
			JFaceResources.getImageRegistry().put(ROOT, getImage(ImageKeys.ROOT_FOLDER.getStringValue()));
		}
		
		if (JFaceResources.getImageRegistry().getDescriptor(STUDENT) == null) {
			JFaceResources.getImageRegistry().put(STUDENT, getImage(ImageKeys.STUDENT.getStringValue()));
		}
		
	}

	private Image getImage(String imageKey) {
		
		URL url = Platform.getBundle(Application.PLUGIN_ID).getEntry(imageKey);
		return ImageDescriptor.createFromURL(url).createImage();
	}
	
	
	@Override
	public Image getImage(Object element) {
		
		String imageKey = STUDENT;
		
		if (element instanceof TreeParent) {
			if ( (element != null) && ((TreeParent) element).getName().equals(GroupViewPart.ROOT_FOLDER_NAME)) {
				imageKey = ROOT;
			} else {

				imageKey = GROUP;
			}
		}
		
		return JFaceResources.getImageRegistry().getDescriptor(imageKey).createImage();
		
	}

	@Override
	public String getText(Object element) {
		
		return element.toString();
		
	}

	
}
