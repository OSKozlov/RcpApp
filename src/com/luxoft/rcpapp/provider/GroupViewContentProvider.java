package com.luxoft.rcpapp.provider;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.luxoft.rcpapp.dto.TreeObject;
import com.luxoft.rcpapp.dto.TreeParent;

public class GroupViewContentProvider implements ITreeContentProvider {

	@Override
	public Object[] getElements (Object inputElement) {
		return getChildren(inputElement);
	}

	@Override
	public Object[] getChildren (Object parentElement) {
	
		if (parentElement instanceof TreeParent) {
			return ((TreeParent) parentElement).getChildren();
		}
		
		return new Object[0];	
	}

	@Override
	public Object getParent (Object element) {
		
		if (element instanceof TreeObject) {
			return ((TreeObject) element).getParent();
		}
		return null;
	}

	@Override
	public boolean hasChildren (Object element) {
		if (element  instanceof TreeParent)
			return ((TreeParent) element).hasChildren();
		return false;
	}

}
